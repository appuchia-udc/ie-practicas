# ###
# 3.
# ###


# a)

n <- 200
c <- 114
conf <- .96
a <- 1-conf

p <- c/n

e1 <- qnorm(a/2, lower.tail=FALSE) * sqrt(p*(1-p)/n)
e2 <- qnorm(a/2, lower.tail=FALSE) * sqrt(1/(4*n))

IC <- c(p-e1, p+e1)
IC
IC <- c(p-e2, p+e2)
IC

mag <- 2*e1


# ###
# 4.
# ###

A <- c(1614, 1094, 1293, 1643, 1466, 1270, 1340, 1380, 1028, 1487)
B <- c(1383, 1138, 1092, 1143, 1017, 1061, 1627, 1021, 1711, 1065)

t.test(A, B, paired=FALSE, var.equal=TRUE, conf=.9)$c

var.test(A,B, conf.level=.9)$c
# 1 pertenece a IC por lo que si

# ###
# 5.
# ###

X <- c(38, 42, 41, 35, 42, 32, 45, 37)
Y <- c(30, 32, 34, 37, 35, 26, 38, 32)

t.test(X, Y, paired=TRUE, var.equal=FALSE)$c


# ###
# 6.
# ###

n <- 100
c <-7
conf <- .99

p <- c/n
e <- qnorm((1-conf)/2, lower.tail=FALSE) * sqrt( p*(1-p)/n )

IC <- c(p-e, p+e)
