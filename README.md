# IE

[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)

## Qué es esto

Aquí está el código que he escrito en las clases de Inferencia Estadística.

La mayor parte de los archivos tienen un PDF autogenerado con mejor estilo.

- El archivo `Apuntes.R` contiene el código de las clases teóricas (2/semana).
- La carpeta `Practica`  contiene el código de la práctica evaluable (20230502).
- La carpeta `Practicas` contiene el código de las clases prácticas (1/semana).

## Licencia

El código está licenciado bajo la [licencia GPLv3](https://gitlab.com/appuchia/appuchia-udc/ie-practicas/-/blob/master/LICENSE).

Coded with 🖤 by Appu
